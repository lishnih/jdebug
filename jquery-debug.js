// stan 2012-02-06


jQuery( function($) {


  // Добавляем элемент 'jdebug' на страницу для вывода отладочных данных
  $("body").append('<div id="jdebug">');
  $('div#jdebug')
    .append('<div id="menu">')
    .append('<div id="log">');


    var myString = function() {/*
<ul id="menu" class="menu">
  <li>
    <span unselectable="on">Меню</span>
    <ul>
      <li><span id="debugmenu_clearall" unselectable="on">Очистить</span></li>
      <li><span id="debugmenu_append_something" unselectable="on">Добавить текст</span></li>
      <li><span id="debugmenu_hide" unselectable="on">Скрыть</span></li>
    </ul>
  </li>
</ul>
*/}.toString().slice(15, -3)

    $('div#jdebug div#menu').html(myString);


  // Register a handler to be called when Ajax requests complete with an error
  $( document ).ajaxError( function(event, jqxhr, settings, thrownError) {
    debug(event);
    debug(jqxhr);
    debug('<span style="color: red"><b><i>ajaxError:</i></b></span>');
  } );


  // Menu actions
  $('.menu li').hover(
    function() {
      $('ul:first', this).stop(true, true);
      $('ul:first', this).slideDown();
    },
    function() {
      $('ul:first', this).stop(true, true);
      $('ul:first', this).slideUp('slow');
    }
  );

  $('.menu li').click(
    function() {
      $('ul:first', this).stop(true, true);
      $('ul:first', this).slideUp('slow');
//    $(this).toggleClass("active");
    }
  );


  // Добавляем значки в подменю
  $(".menu li ul li:has(ul)").find("span:first").append(" &raquo; ");


  // Биндим функции к элементам меню

  $('#debugmenu_clearall').click( function() {
    debug();
  } );

  $('#debugmenu_append_something').click( function() {
    debug('print something...');
  } );

  $('#debugmenu_hide').click( function() {
    $('div#jdebug').hide();
  } );


} ); // jQuery( function($)


// Вывод отладочных данных
// Очищает поток, если вызвать без параметра 'message'
function debug(message) {
  if (typeof message == "undefined") {
    $('div#jdebug div#log').text('');
  } else {
    message = var_dump1(message);
    $('<div/>')
      .html(message)
      .prependTo('div#jdebug div#log');
    $('div#jdebug').show();
  }
} // function debug(message)
